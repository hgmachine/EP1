Henrique G Moura - 16/0058651
Disciplina de Orienta��o a Objeto

----------------------------------------------------------------------------------------------
Projeto EP1: Notas ao professor
----------------------------------------------------------------------------------------------

- O projeto j� havia sido iniciado sem controle de arquivos, mas a partir da data de registro
poder� ser acompanhado/consultado normalmente.

- O este projeto ser� realizado utilizando o ambiente integrado CodeBlocks (c++) para
melhor aplica��o de estrat�gias "Debug" e consequentemente melhor aproveitamento de tempo.
Tenho interesse em aprender como realizar o "Debug" em linha de comando mas acho prudente
acrescentar este desafio neste est�gio.

- O arquivo "Projeto_EP1_HGM" contem o UML para o EP1 para melhor entedimento

- todos os arquivos de c�digo est�o na pasta imagemain1, constitu�da automaticamente pelo CodeBlocks

- Aten��o para a cria��o do arquivo .gitignore.