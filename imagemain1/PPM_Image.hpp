#ifndef PPM_IMAGE_HPP_INCLUDED
#define PPM_IMAGE_HPP_INCLUDED

#include<iostream>
#include<fstream>
#include<string>

using std::cin;
using std::cout;
using std::cerr;
using std::ios;
using std::endl;
using std::ofstream;
using std::ifstream;
using std::string;

class PPM_Image {
public:
    PPM_Image();
    ~PPM_Image();
    bool isOpen();
    void setInputFile(string);
    void setOutputFile(string);
protected:
    ifstream image_from_ppm;
    ofstream image_to_ppm;
    string input;
    string output;
    bool status;
};

#endif // PPM_IMAGE_HPP_INCLUDED
