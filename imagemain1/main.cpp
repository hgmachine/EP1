// HGM: 16/0058651
// Data: 24/09/16
// Leitura e gravacao de uma imagem em formato PPM

#include<iostream>
#include<fstream>
#include<string>

#include "PPM_Data.hpp"
#include "ppm_negt.hpp"
#include "ppm_bnwh.hpp"
#include "ppm_polar.hpp"
#include "ppm_mean.hpp"
#include "Workspace.hpp"

using std::cin;
using std::cout;
using std::cerr;
using std::ios;
using std::endl;
using std::ofstream;
using std::ifstream;
using std::string;

int main()
{
    Workspace menu;

    menu.options();

    return 0;
}
