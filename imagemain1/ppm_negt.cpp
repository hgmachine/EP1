// Defini��o de fun��es membro da classe ppm_negt

#include "ppm_negt.hpp"

ppm_negt::ppm_negt()
{
    id= "negative";
    tag= "This image has been inverted";
}

ppm_negt::~ppm_negt()
{

}

string ppm_negt::what()
{
    return tag;
}

string ppm_negt::which()
{
    return id;
}

void ppm_negt::operate(PPM_Data *image)
{
    int i, raux,gaux,baux;
    long int *r= image->getRedColors();
    long int *g= image->getGreenColors();
    long int *b= image->getBlueColors();
    long int np= image->getPels();
    int mt= image->getMax();

    if (image->isOpen())
    {
        for (i = 0; i < np; i++)
        {
            raux= mt - r[i];
            gaux= mt - g[i];
            baux= mt - b[i];

            r[i]= raux;
            g[i]= gaux;
            b[i]= baux;
        }
    }
}
