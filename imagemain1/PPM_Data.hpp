#ifndef PPM_DATA_HPP_INCLUDED
#define PPM_DATA_HPP_INCLUDED

#include<iostream>
#include<fstream>
#include<string>
#include<stdlib.h>

using std::cin;
using std::cout;
using std::cerr;
using std::ios;
using std::endl;
using std::ofstream;
using std::ifstream;
using std::string;

#include "PPM_Image.hpp"

class PPM_Data : public PPM_Image {
friend int *atoij(const char *pchar);
public:
    PPM_Data();
    ~PPM_Data();
    int download();
    int upload(string);
    // getters
    int getY();
    int getX();
    int getMax();
    long int getPels();
    long int *getRedColors();
    long int *getGreenColors();
    long int *getBlueColors();
protected:
    int *yx;
    int  mpixel;
    long int npixels;
    long int *red;
    long int *green;
    long int *blue;
    void clearall();
    void initialization();
};

#endif // PPM_DATA_HPP_INCLUDED
