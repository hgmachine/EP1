// Defini��o de fun��es membro da classe Filter

#include<iostream>
#include<fstream>
#include<string>

using std::cin;
using std::cout;
using std::cerr;
using std::ios;
using std::endl;
using std::ofstream;
using std::ifstream;
using std::string;

#include "Filter.hpp"

Filter::Filter()
{
    id= "all-pass";
    tag= "This image correspond to original";
}

Filter::~Filter()
{
    // destrutor
}

void Filter::operate(PPM_Data *image)
{
    cout << "This is an all-pass filter";
}

string Filter::what()
{
    return tag;
}

string Filter::which()
{
    return id;
}
