#ifndef WORKSPACE_HPP_INCLUDED
#define WORKSPACE_HPP_INCLUDED

#include<iostream>
#include<fstream>
#include<string>

using std::cin;
using std::cout;
using std::cerr;
using std::ios;
using std::endl;
using std::ofstream;
using std::ifstream;
using std::string;

#include "ppm_bnwh.hpp"
#include "ppm_mean.hpp"
#include "ppm_negt.hpp"
#include "ppm_polar.hpp"
#include "PPM_Data.hpp"

class Workspace {
public:
    Workspace();
    ~Workspace();
    void options();
    void execute();
    void setInput(string);
    void setOutput(string);
    void setFilter(int);
private:
    int wfilter;
    bool win;
    bool wout;
    string input_file;
    string output_file;
    PPM_Data  *ppmdata;
    ppm_negt  *pneg;
    ppm_mean  *pavg;
    ppm_polar *ppol;
    ppm_bnwh  *pbnw;
    void clearall();
    void clearfilters();
    void clearimage();
    void initialization();
};

#endif // WORKSPACE_HPP_INCLUDED
