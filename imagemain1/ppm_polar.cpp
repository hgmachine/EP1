// Defini��o de fun��es membro da classe ppm_polar

#include "ppm_polar.hpp"

ppm_polar::ppm_polar()
{
    id= "polarization";
    tag= "This image has been polarizated";
}

ppm_polar::~ppm_polar()
{

}

string ppm_polar::what()
{
    return tag;
}

string ppm_polar::which()
{
    return id;
}

void ppm_polar::operate(PPM_Data *image)
{
    int i, raux,gaux,baux;
    long int *r= image->getRedColors();
    long int *g= image->getGreenColors();
    long int *b= image->getBlueColors();
    long int np= image->getPels();
    int mt= image->getMax();

    if (image->isOpen())
    {
        for (i = 0; i < np; i++)
        {
            raux= (r[i]<(mt/2))?0:mt;
            gaux= (g[i]<(mt/2))?0:mt;
            baux= (b[i]<(mt/2))?0:mt;

            r[i]= raux;
            g[i]= gaux;
            b[i]= baux;
        }
    }
}
