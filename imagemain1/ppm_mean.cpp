// Defini��o de fun��es membro da classe ppm_mean

#include<math.h>
#include "ppm_mean.hpp"

ppm_mean::ppm_mean()
{
    Filter::id= "mean";
    Filter::tag= "This image has been averaged";
}

ppm_mean::~ppm_mean()
{
    // destrutor
}

string ppm_mean::what()
{
    return tag;
}

string ppm_mean::which()
{
    return id;
}

void ppm_mean::operate(PPM_Data *image)
{
    int x,y;
    float z;
    int i, j, raux,gaux,baux;
    long int *r= image->getRedColors();
    long int *g= image->getGreenColors();
    long int *b= image->getBlueColors();

    int m= image->getX();
    int n= image->getY();

    if (image->isOpen())
    {
         for (i = (m+2); i <= (m*(n-1)-1); i++)
        {
            y= floor(i/m);
            x= i-y*m-1;
            if (x>0)
            {
                j= i-1;

                z= r[j-m-1]+r[j-m]+r[j-m+1]+r[j-1]+r[j]+r[j+1]+r[j+m-1]+r[j+m]+r[j+m+1];
                raux= floor(z/9);

                z= g[j-m-1]+g[j-m]+g[j-m+1]+g[j-1]+g[j]+g[j+1]+g[j+m-1]+g[j+m]+g[j+m+1];
                gaux= floor(z/9);

                z= b[j-m-1]+b[j-m]+b[j-m+1]+b[j-1]+b[j]+b[j+1]+b[j+m-1]+b[j+m]+b[j+m+1];
                baux= floor(z/9);

                r[i]= raux;
                g[i]= gaux;
                b[i]= baux;

                //conectividade testada e correta! Para verificar, testa com mp=5 e np=6
                //printf("O pixel %d esta conectado as pixels %d %d %d %d %d %d %d %d \n",j,j-mp-1,j-mp,j-mp+1,j-1,j+1,j+mp-1,j+mp,j+mp+1);
            }
        }
    }
}
