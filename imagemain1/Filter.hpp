#ifndef FILTER_HPP_INCLUDED
#define FILTER_HPP_INCLUDED

#include "PPM_Data.hpp"

class Filter {
public:
    Filter();
    virtual ~Filter();
    virtual void operate(PPM_Data *);
    virtual string what();
    virtual string which();
protected:
    string id;
    string tag;
};

#endif // FILTER_HPP_INCLUDED
