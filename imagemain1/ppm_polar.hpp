#ifndef PPM_POLAR_HPP_INCLUDED
#define PPM_POLAR_HPP_INCLUDED

#include<iostream>
#include<fstream>
#include<string>

using std::cin;
using std::cout;
using std::cerr;
using std::ios;
using std::endl;
using std::ofstream;
using std::ifstream;
using std::string;

#include "Filter.hpp"

class ppm_polar : public Filter {
public:
    ppm_polar();
    virtual ~ppm_polar();
    virtual void operate(PPM_Data *);
    virtual string what();
    virtual string which();
};

#endif // PPM_POLAR_HPP_INCLUDED
