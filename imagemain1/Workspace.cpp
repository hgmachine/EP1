// Defini��o de fun��es membro da classe Workspace

#include "Workspace.hpp"

Workspace::Workspace()
{
    initialization();
}

Workspace::~Workspace()
{
    clearimage();
    clearfilters();
}

void Workspace::clearfilters()
{
    delete pneg;
    delete ppol;
    delete pbnw;
    delete pavg;
}

void Workspace::clearimage()
{
    delete ppmdata;
}

void Workspace::initialization()
{
    win= false;
    wout= false;
    input_file= "none";
    output_file= "none";
    wfilter= 0;
    ppmdata= 0;
    pavg= 0;
    pbnw= 0;
    ppol= 0;
    pneg= 0;
    pneg= new ppm_negt;
    pbnw= new ppm_bnwh;
    pavg= new ppm_mean;
    ppol= new ppm_polar;
}

void Workspace::clearall()
{
    clearimage();
    clearfilters();
    initialization();
}

void Workspace::setFilter(int id)
{
    wfilter= id;
}

void Workspace::setInput(string name)
{
    win= true;
    input_file= name;
}

void Workspace::setOutput(string name)
{
    wout= true;
    output_file= name;
}

void Workspace::execute()
{
    string tag;

    if( win && wout )
    {
        clearimage();
        ppmdata= new PPM_Data;
        ppmdata->setInputFile(input_file);
        ppmdata->download();
        switch (wfilter)
        {
            case 1:
                pneg->operate(ppmdata);
                tag= pneg->what();
                break;
            case 2:
                pbnw->operate(ppmdata);
                tag= pbnw->what();
                break;
            case 3:
                pavg->operate(ppmdata);
                tag= pavg->what();
                break;
            case 4:
                ppol->operate(ppmdata);
                tag= ppol->what();
                break;
        }
        ppmdata->setOutputFile(output_file);
        ppmdata->upload(tag);
    }
}

void Workspace::options()
{
    int option=0, id;
    string input, output;

	while (option!=6)
	{
        //Menu de op��es:
	    system("cls");
		cout << "----------- Ambiente para Aplica��o de Filtros em Imagens *.PMM -------------" << endl << endl;
		if (wfilter)
        {
            cout << "Filtro Ativo: ";
            switch (wfilter)
            {
                case 1:
                    cout << "Negativo" << endl << endl;
                    break;
                case 2:
                    cout << "Preto e Branco" << endl << endl;
                    break;
                case 3:
                    cout << "M�dias 3x3" << endl << endl;
                    break;
                case 4:
                    cout << "Polarizador" << endl << endl;
                    break;
            }
        }

        if (win)
        {
            cout << "Imagem Carregada: " << input_file << endl << endl;
        }

        if (wout)
        {
            cout << "Imagem de Saida: " << output_file << endl << endl;
        }

		cout << "Digite uma entre as op��es" << endl << endl;
		cout << "1 - Selecionar arquivo da imagem a ser lida" << endl;
		cout << "2 - Selecionar arquivo da imagem a ser gravada" << endl;
		cout << "3 - Selecionar filtro para imagem" << endl;
		cout << "4 - Executar filtragem" << endl;
		cout << "5 - Limpar tudo" << endl;
	    cout << "6 - Sair" << endl << endl;
		cout << "Opcao: ";
		cin >> option;
		if((option>0)&&(option<7))
		{
		    cout << "-----------------------------------------------------------------------------" << endl;
            switch (option)
            {
                case 1:
                    cout << "Digite o nome do arquivo: ";
                    cin >> input;
                    setInput(input);
                    break;
                case 2:
                    cout << "Digite o nome do arquivo: ";
                    cin >> output;
                    setOutput(output);
                    break;
                case 3:
                    id= 0;
                    while(id!=5)
                    {
                        cout << "Digite o nome do filtro: " << endl;
                        cout << "1 - negativo" << endl;
                        cout << "2 - preto e branco" << endl;
                        cout << "3 - m�dias (3x3)" << endl;
                        cout << "4 - polarizador" << endl;
                        cout << "5 - cancelar operacao" << endl;
                        cout << "Opcao: ";
                        cin >> id;
                        if ((id>0)&&(id<6))
                        {
                            setFilter(id);
                            id= 5;
                        }
                    }
                    break;
                case 4:
                    if ( (!win) || (!wout) )
                    {
                        cout << "Defina primeiramente nomes para os arquivos de leitura e saida." << endl;
                        system("pause");
                    }
                    else
                        execute();
                    break;
                case 5:
                    clearall();
                    break;
                case 6:
                    cout << "Exercicio Pratico 1 - OOFGA - HGM - 24/09/16" << endl;
                    break;
            }
        }
    }
}
