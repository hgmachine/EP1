// Defini��o de fun��es membro da classe ppm_bnwh

#include "ppm_bnwh.hpp"

ppm_bnwh::ppm_bnwh()
{
    id= "black and white";
    tag= "This image has been turned to gray-only scales";
}

ppm_bnwh::~ppm_bnwh()
{
    id= "black and white";
    tag= "This image has been turned to gray-only scales";
}

string ppm_bnwh::what()
{
    return tag;
}

string ppm_bnwh::which()
{
    return id;
}

void ppm_bnwh::operate(PPM_Data *image)
{
    int i, raux,gaux,baux;
    long int *r= image->getRedColors();
    long int *g= image->getGreenColors();
    long int *b= image->getBlueColors();
    long int np= image->getPels();

    if (image->isOpen())
    {
        for (i = 0; i < np; i++)
        {
            raux= 0.299*r[i] + 0.587*g[i] + 0.144*b[i];
            gaux= raux;
            baux= raux;

            r[i]= raux;
            g[i]= gaux;
            b[i]= baux;
        }
    }
}
