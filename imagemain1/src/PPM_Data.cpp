// Defini��o de fun��es membro da classe PPM_Data

#include<iostream>
#include<fstream>
#include<string>

using std::cin;
using std::cout;
using std::cerr;
using std::ios;
using std::endl;
using std::ofstream;
using std::ifstream;
using std::string;

#include "PPM_Data.hpp"

PPM_Data::PPM_Data()
{
    npixels= 0;
    mpixel= 255;
    yx= nullptr;
    red= nullptr;
    green= nullptr;
    blue= nullptr;
}

PPM_Data::~PPM_Data()
{
    delete yx;
    delete red;
    delete green;
    delete blue;
}

int PPM_Data::download()
{
    long int j= -1;
    string buffer;

    if (!open())
    {
        cerr << "Arquivo de leitura nao pode ser aberto ou nao foi encontrado!";
        exit(1);
    }

    getline(image_from_ppm,buffer);
    if (buffer.compare("P3"))
    {
        cout << "O arquivo de imagem a ser lido nao esta em formato adequado : PPM >> ASCII";
        exit(1);
    }

    while(getline(image_from_ppm,buffer))
        if (buffer.find("#")) { break; }

    yx = atoij(buffer.c_str());
    getline(image_from_ppm,buffer);
    mpixel = atoi(buffer.c_str());
    npixels = yx[0]*yx[1]);

    if ((red==nullptr)&&(np>0))
    {
        red= new long int[np];
        green= new long int[np];
        blue= new long int[np];
    }

    for (i = 0; i < np; i++)
    {
        j++;
        getline(image_from_ppm,buffer);
        red[j] = atoi(buffer.c_str());
        getline(image_from_ppm,buffer);
        green[j] = atoi(buffer.c_str());
        getline(image_from_ppm,buffer);
        blue[j] = atoi(buffer.c_str());
    }

    return 0;
}

int PPM_Data::upload(string tag)
{
    if (status)
    {
        image_to_ppm_mea << "P3\n";
        image_to_ppm_mea << tag;
        image_to_ppm_mea << yx[0] << " " << yx[1] << "\n";
        image_to_ppm_mea << mpixel << "\n";

        for (i = 0; i < npixels; i++)
        {
            image_to_ppm_bnw << red[i] << endl;
            image_to_ppm_bnw << green[i] << endl;
            image_to_ppm_bnw << blue[i] << endl;
        }
    }
    else
    {
        cout << "We highly-suggest to download the image from disk before uploada any data." << endl;
        exit(1);
    }

    return 0;
}

int PPM_Data::getX()
{
    if (status)
    {
        return yx[1];
    }
    else
    {
        cout << "image Data does not exists." << endl;
        exit(1);
    }
}

int PPM_Data::getY()
{
    if (status)
    {
        return yx[0];
    }
    else
    {
        cout << "image Data does not exists." << endl;
        exit(1);
    }
}

int PPM_Data::getMax()
{
    if (status)
    {
        return mpixel;
    }
    else
    {
        cout << "image Data does not exists." << endl;
        exit(1);
    }
}

long int PPM_Data::getPels()
{
    if (status)
    {
        return npixels;
    }
    else
    {
        cout << "image Data does not exists." << endl;
        exit(1);
    }
}

long int * PPM_Data::getRedColors()
{
    if (status)
    {
        return red;
    }
    else
    {
        cout << "image Data does not exists." << endl;
        exit(1);
    }
}

long int * PPM_Data::getGreenColors()
{
    if (status)
    {
        return green;
    }
    else
    {
        cout << "image Data does not exists." << endl;
        exit(1);
    }
}

long int * PPM_Data::getBlueColors()
{
    if (status)
    {
        return blue;
    }
    else
    {
        cout << "image Data does not exists." << endl;
        exit(1);
    }
}

int *atoij(const char *pchar)
{
    int i=-1,j=-1,kk;
    char maux[21];
    int *vec= new int[2];

    for (kk=0;kk<21;kk++) {maux[kk]=' ';}

    while(pchar[++i]==' ') {}

    while(pchar[i]!=' ')
    {
        maux[++j]= pchar[i++];
    }

    vec[0]= atoi(maux);
    for (kk=0;kk<21;kk++) {maux[kk]=' ';}
    j=-1;

    while(pchar[++i]==' ') {}

    while(pchar[i]!='\0')
    {
        maux[++j]= pchar[i++];
    }

    vec[1]= atoi(maux);

    return vec;
}
