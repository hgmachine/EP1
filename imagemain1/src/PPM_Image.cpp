// Defini��o de fun��es membro da classe PPM_Image

#include<iostream>
#include<fstream>
#include<string>

using std::cin;
using std::cout;
using std::cerr;
using std::ios;
using std::endl;
using std::ofstream;
using std::ifstream;
using std::string;

#include "PPM_Image.hpp"

PPM_Image::PPM_Image()
{
    image_from_ppm= nullptr;
    image_to_ppm= nullptr;
    input= "xxx.xxx";
    output= "xxx.xxx";
    status= 0;
}

PPM_Image::~PPM_Image()
{
    image_from_ppm.close();
}

void PPM_Image::open()
{
    image_from_ppm.open(input);
    image_to_ppm.open(output);
    status= (image_from_ppm.is_open()) && (image_to_ppm.is_open());
    return static_cast<int>(status);
}

void PPM_Image::setInputFile(string name)
{
    input= name;
}

void PPM_Image::setOutputFile(string name)
{
    output= name;
}

void PPM_Image::isOpen()
{
    return status;
}
