// Defini��o de fun��es membro da classe ppm_mean

#include<iostream>
#include<fstream>
#include<string>

using std::cin;
using std::cout;
using std::cerr;
using std::ios;
using std::endl;
using std::ofstream;
using std::ifstream;
using std::string;

#include "ppm_mean.hpp"

ppm_mean::ppm_mean()
{
    id= "mean";
    tag= "This image has been averaged";
}

void ppm_mean::what()
{
    return tag;
}

void ppm_mean::which()
{
    return id;
}

void ppm_mean::operate(PPM_Data *image)
{
    float z;
    int i, j, raux,gaux,baux;
    long int *r= image.getRedColors();
    long int *g= image.getGreenColors();
    long int *b= image.getBlueColors();
    long int np= image.getPels();
    int mt= image.getMax();

    int mp= image.getX();
    int np= image.getY();

    if (image.isOpen())
    {
         for (i = (mp+2); i <= (mp*(np-1)-1); i++)
        {
            y= floor(i/mp);
            x= i-y*mp-1;
            if (x>0)
            {
                j= i-1;

                z= r[j-mp-1]+r[j-mp]+r[j-mp+1]+r[j-1]+r[j]+r[j+1]+r[j+mp-1]+r[j+mp]+r[j+mp+1];
                raux= floor(z/9);

                z= g[j-mp-1]+g[j-mp]+g[j-mp+1]+g[j-1]+g[j]+g[j+1]+g[j+mp-1]+g[j+mp]+g[j+mp+1];
                gaux= floor(z/9);

                z= b[j-mp-1]+b[j-mp]+b[j-mp+1]+b[j-1]+b[j]+b[j+1]+b[j+mp-1]+b[j+mp]+b[j+mp+1];
                baux= floor(z/9);

                r[i]= raux;
                g[i]= gaux;
                b[i]= baux;

                //conectividade testada e correta! Para verificar, testa com mp=5 e np=6
                //printf("O pixel %d esta conectado as pixels %d %d %d %d %d %d %d %d \n",j,j-mp-1,j-mp,j-mp+1,j-1,j+1,j+mp-1,j+mp,j+mp+1);
            }
        }
    }
}
