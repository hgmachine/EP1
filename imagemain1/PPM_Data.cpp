// Defini��o de fun��es membro da classe PPM_Data

#include "PPM_Data.hpp"

int *atoij(const char *pchar)
{
    int i=-1,j=-1,kk;
    char maux[21];
    int *vec= new int[2];

    for (kk=0;kk<21;kk++) {maux[kk]=' ';}

    while(pchar[++i]==' ') {}

    while(pchar[i]!=' ')
    {
        maux[++j]= pchar[i++];
    }

    vec[0]= atoi(maux);
    for (kk=0;kk<21;kk++) {maux[kk]=' ';}
    j=-1;

    while(pchar[++i]==' ') {}

    while(pchar[i]!='\0')
    {
        maux[++j]= pchar[i++];
    }

    vec[1]= atoi(maux);

    return vec;
}

PPM_Data::PPM_Data()
{
   initialization();
}

PPM_Data::~PPM_Data()
{
    clearall();
}

int PPM_Data::download()
{
    long int i;
    string buffer;

    if (status)
    {
        clearall();
        initialization();
    }

    image_from_ppm.open(input.c_str());
    status= image_from_ppm.is_open();

    if (!status)
    {
        cerr << "Arquivo de leitura nao pode ser aberto ou nao foi encontrado!";
        exit(1);
    }

    getline(image_from_ppm,buffer);

    if (buffer.compare("P3"))
    {
        cout << "O arquivo de imagem a ser lido nao esta em formato adequado : PPM >> ASCII";
        exit(1);
    }

    while(getline(image_from_ppm,buffer))
        if (buffer.find("#")) { break; }

    yx = atoij(buffer.c_str());
    getline(image_from_ppm,buffer);
    mpixel = atoi(buffer.c_str());
    npixels = yx[0]*yx[1];

    if ((red==0)&&(npixels>0))
    {
        red= new long int[npixels];
        green= new long int[npixels];
        blue= new long int[npixels];
    }

    for (i = 0; i < npixels; i++)
    {
        getline(image_from_ppm,buffer);
        red[i] = atoi(buffer.c_str());
        getline(image_from_ppm,buffer);
        green[i] = atoi(buffer.c_str());
        getline(image_from_ppm,buffer);
        blue[i] = atoi(buffer.c_str());
    }

    return 0;
}

int PPM_Data::upload(string tag)
{
    long int i;

    if (status)
    {
        image_to_ppm.open(output.c_str());

        if(image_to_ppm.is_open())
        {
            image_to_ppm << "P3" << endl;
            image_to_ppm << "#" << tag << endl;
            image_to_ppm << yx[0] << " " << yx[1] << endl;
            image_to_ppm << mpixel << endl;

            for (i = 0; i < npixels; i++)
            {
                image_to_ppm << red[i] << endl;
                image_to_ppm << green[i] << endl;
                image_to_ppm << blue[i] << endl;
            }
        }
        else
        {
            cout << "Output file could not be opened." << endl;
            exit(1);
        }
    }
    else
    {
        cout << "We highly-suggest to download the image from disk before uploada any data." << endl;
        exit(1);
    }

    return 0;
}

int PPM_Data::getX()
{
    if (status)
    {
        return yx[1];
    }
    else
    {
        cout << "image Data does not exists." << endl;
        exit(1);
    }
}

int PPM_Data::getY()
{
    if (status)
    {
        return yx[0];
    }
    else
    {
        cout << "image Data does not exists." << endl;
        exit(1);
    }
}

int PPM_Data::getMax()
{
    if (status)
    {
        return mpixel;
    }
    else
    {
        cout << "image Data does not exists." << endl;
        exit(1);
    }
}

long int PPM_Data::getPels()
{
    if (status)
    {
        return npixels;
    }
    else
    {
        cout << "image Data does not exists." << endl;
        exit(1);
    }
}

long int *PPM_Data::getRedColors()
{
    if (status)
    {
        return red;
    }
    else
    {
        cout << "image Data does not exists." << endl;
        exit(1);
    }
}

long int *PPM_Data::getGreenColors()
{
    if (status)
    {
        return green;
    }
    else
    {
        cout << "image Data does not exists." << endl;
        exit(1);
    }
}

long int *PPM_Data::getBlueColors()
{
    if (status)
    {
        return blue;
    }
    else
    {
        cout << "image Data does not exists." << endl;
        exit(1);
    }
}

void PPM_Data::clearall()
{
    image_from_ppm.close();
    image_to_ppm.close();

    delete yx;
    delete red;
    delete green;
    delete blue;
}

void PPM_Data::initialization()
{
    npixels= 0;
    mpixel= 255;
    yx= 0;
    red= 0;
    green= 0;
    blue= 0;
}
