#ifndef PPM_MEAN_HPP_INCLUDED
#define PPM_MEAN_HPP_INCLUDED

#include<iostream>
#include<fstream>
#include<string>

using std::cin;
using std::cout;
using std::cerr;
using std::ios;
using std::endl;
using std::ofstream;
using std::ifstream;
using std::string;

#include "Filter.hpp"

class ppm_mean : public Filter {
public:
    ppm_mean();
    virtual ~ppm_mean();
    virtual void operate(PPM_Data *);
    virtual string what();
    virtual string which();
};

#endif // PPM_MEAN_HPP_INCLUDED
